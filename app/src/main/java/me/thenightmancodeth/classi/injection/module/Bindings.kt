/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/

package me.thenightmancodeth.classi.injection.module

import dagger.Binds
import dagger.Module
import me.thenightmancodeth.classi.data.DataManager
import me.thenightmancodeth.classi.data.DataManagerImpl

@Module
abstract class Bindings {
    @Binds
    internal abstract fun bindDataManager(manager: DataManagerImpl): DataManager
}