/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.injection.component

import dagger.Subcomponent
import me.thenightmancodeth.classi.injection.scope.PerActivity
import me.thenightmancodeth.classi.ui.main.MainActivity
import me.thenightmancodeth.classi.ui.settings.main.SettingsActivity
import me.thenightmancodeth.classi.ui.settings.theme.ThemeSettingsActivity

@PerActivity
@Subcomponent
interface ActivityComponent {
    fun inject(activity: MainActivity)
    fun inject(activity: SettingsActivity)
    fun inject(activity: ThemeSettingsActivity)
}