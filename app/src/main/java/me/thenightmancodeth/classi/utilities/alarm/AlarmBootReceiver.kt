/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.utilities.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import io.realm.Realm
import me.thenightmancodeth.classi.data.model.Class
import me.thenightmancodeth.classi.utilities.extensions.scheduleAlarm

class AlarmBootReceiver: BroadcastReceiver() {
    override fun onReceive(ctx: Context?, intent: Intent?) {
        Log.i("Classi.Alarm", "Boot received")
        if (intent?.action.equals("android.intent.action.BOOT_COMPLETED")) {
            Realm.init(ctx)
            val realm: Realm = Realm.getDefaultInstance()
            realm.where(Class::class.java).findAll().forEach { clazz ->
                clazz.days.toCharArray().forEach {
                    it.scheduleAlarm(ctx!!, clazz)
                }
            }
        }
    }
}