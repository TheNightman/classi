/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.utilities.alarm

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v7.app.NotificationCompat
import android.util.Log
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.ui.main.MainActivity

/**
 *  This receiver must be declared in AndroidManifest, and is called when a class/assignment alarm
 *  is set off
 **/
class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(ctx: Context?, intent: Intent?) {
        //Extras will contain the title and details to populate the notification with
        val extras = intent?.extras

        Log.i("Classi.AlarmReceiver", "Alarm received")

        val notiIntent: Intent = Intent(ctx, MainActivity::class.java)

        /**
         * The stackbuilder object holds an artificial backstack for the launched activity.
         * This will make sure that when the user presses 'back' from the launched activity,
         * they will be brought back to the homescreen.
         */
        val stackBuilder: TaskStackBuilder = TaskStackBuilder.create(ctx)
        stackBuilder.addParentStack(MainActivity::class.java)
        stackBuilder.addNextIntent(notiIntent)
        val resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val notiBuilder: NotificationCompat.Builder =
                NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.ic_noti)
                        .setContentTitle(extras?.getString("title"))
                        .setContentText(extras?.getString("content"))
                        .setContentIntent(resultPendingIntent)
                        as android.support.v7.app.NotificationCompat.Builder

        val notifyID = 1
        val notiMan  = ctx
                ?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notiMan.notify(notifyID, notiBuilder.build())
    }
}