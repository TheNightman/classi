/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.utilities.extensions

import android.content.Context
import android.view.View
import android.widget.CheckBox

fun Int.dpToPx(context: Context): Int = this * context.resources.displayMetrics.density.toInt()
fun View.show() {
    this.visibility = View.VISIBLE
}
fun View.hide() {
    this.visibility = View.GONE
}
fun List<CheckBox>.toDaysString(): String {
    var days: String = ""
    this.forEach {
        if (it.isChecked) {
            when(it.text.toString()) {
                "SUN" -> days += "U"
                "MON" -> days += "M"
                "TUE" -> days += "T"
                "WED" -> days += "W"
                "THU" -> days += "R"
                "FRI" -> days += "F"
                "SAT" -> days += "S"
            }
        }
    }
    return days
}