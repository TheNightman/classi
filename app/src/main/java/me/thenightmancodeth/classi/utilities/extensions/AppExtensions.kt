/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.utilities.extensions

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import me.thenightmancodeth.classi.App
import me.thenightmancodeth.classi.data.model.Class
import me.thenightmancodeth.classi.injection.component.AppComponent
import me.thenightmancodeth.classi.utilities.alarm.AlarmReceiver
import java.util.*

/**
 * Converts a char (UMTWRFS) to a Calendar.DAY int
 */
fun Char.toDayInt(): Int {
    when(this) {
        'U' -> return Calendar.SUNDAY
        'M' -> return Calendar.MONDAY
        'T' -> return Calendar.TUESDAY
        'W' -> return Calendar.WEDNESDAY
        'R' -> return Calendar.THURSDAY
        'F' -> return Calendar.FRIDAY
    }
    return -1
}

/**
 * Schedules weekly alarm for day char (UMTWRFS)
 */
fun Char.scheduleAlarm(context: Context, clazz: Class) {
    val day = this.toDayInt()

    // Make a calendar object with the given time
    val cal: Calendar = Calendar.getInstance()
    cal.set(Calendar.DAY_OF_WEEK, day)
    cal.set(Calendar.HOUR_OF_DAY, clazz.time.splitTime()[0]) //Hour *has* to be 24hr
    cal.set(Calendar.MINUTE, clazz.time.splitTime()[1])

    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    // This intent is used to create the PendingIntent launched when the alarm is triggered
    val intent: Intent = Intent(context, AlarmReceiver::class.java)
    intent.putExtra("title", clazz.name)
    intent.putExtra("content", "${clazz.professor} | ${clazz.location}")

    val receiver = PendingIntent.
            getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)

    // Set repeating Real Time Clock alarm and wakeup to ensure notification is shown
    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.timeInMillis,
            AlarmManager.INTERVAL_DAY*7, receiver)

    Log.i("Classi.AlarmMgr", "Alarm set ${cal.timeInMillis}")
}

/**
 * Takes a time string (HR:MIN) and returns an array with the hour and minute int values
 */
fun String.splitTime(): Array<Int> {
    var hr: Int = this.substringBefore(':').toInt()
    when(hr) {
        23 -> hr = 23
        else -> hr -= 1
    }
    val min: Int = this.substringAfter(':').toInt()
    return arrayOf(hr, min)
}

/**
 * Takes a time string in 24hr format and returns a 12hr format string
 */
fun String.to12Hr(): String {
    var hr: Int = this.substringBefore(':').toInt()
    if (hr > 12) {
        hr -= 12
    }
    return "$hr:${this.substringAfter(':')}"
}

// Returns appcomponent anywhere there is a context available
fun Context.getAppComponent(): AppComponent = (applicationContext as App).appComponent
