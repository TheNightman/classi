/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.settings.main

import android.content.Intent
import io.reactivex.Observable
import me.thenightmancodeth.classi.injection.scope.PerActivity
import me.thenightmancodeth.classi.ui.base.BasePresenter
import io.reactivex.schedulers.Schedulers.io
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.ui.main.MainActivity
import me.thenightmancodeth.classi.ui.settings.theme.ThemeSettingsActivity

import javax.inject.Inject

@PerActivity
class SettingsPresenter @Inject constructor() : BasePresenter<SettingsView>() {
    fun getSettings() {
        val settings = listOf(
                Pair(Pair("Alert Settings", "Settings for class and assignment alerts"),
                        R.drawable.ic_notifications_active_black_24dp),
                Pair(Pair("Look and Feel","Change theme settings"),
                        R.drawable.ic_color_lens_black_24dp),
                Pair(Pair("More to come!", ""),
                        R.drawable.ic_assignment_black_24dp)
        )

        disposables.add(
                Observable.just(settings)
                        .subscribeOn(io())
                        .observeOn(mainThread())
                        .subscribe (
                            { view?.onFetchSettingsSuccess(it) },
                            { view?.onFetchSettingsError(it) }
                        )
        )

    }
}