/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.afollestad.aesthetic.AestheticActivity
import me.thenightmancodeth.classi.injection.component.ActivityComponent
import me.thenightmancodeth.classi.utilities.extensions.getAppComponent

abstract class BaseActivity : AestheticActivity() {
    protected var activityComponent: ActivityComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())
        activityComponent = getAppComponent().activityComponent()
    }

    override fun onDestroy() {
        activityComponent = null
        super.onDestroy()
    }

    protected abstract fun getLayoutResId(): Int
}