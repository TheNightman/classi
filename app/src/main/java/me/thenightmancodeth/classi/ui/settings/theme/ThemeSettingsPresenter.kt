/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.settings.theme

import android.support.v4.app.FragmentManager
import android.view.View
import com.afollestad.aesthetic.Aesthetic
import com.kunzisoft.androidclearchroma.ChromaDialog
import com.kunzisoft.androidclearchroma.IndicatorMode
import com.kunzisoft.androidclearchroma.colormode.ColorMode
import com.kunzisoft.androidclearchroma.listener.OnColorSelectedListener
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.injection.scope.PerActivity
import me.thenightmancodeth.classi.ui.base.BasePresenter
import me.thenightmancodeth.classi.ui.main.custom.CircleImageView
import javax.inject.Inject

@PerActivity
class ThemeSettingsPresenter @Inject constructor() : BasePresenter<ThemeSettingsView>() {
    fun getThemeSettings(settings: List<Pair<Pair<String, String>, Pair<View.OnClickListener, Int>>>) {
        disposables.add(
                Observable.just(settings)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe (
                                { view?.onFetchThemeSettingsSuccess(it) },
                                { view?.onFetchThemeSettingsError(it) }
                        )
        )
    }

    fun showColorPicker(fm: FragmentManager, view: CircleImageView, which: Int) {
        val colorPicker = ChromaDialog.Builder()
                .initialColor(Aesthetic.get().colorPrimary().take(1).blockingFirst())
                .colorMode(ColorMode.RGB) // RGB, ARGB, HVS, CMYK, CMYK255, HSL
                .indicatorMode(IndicatorMode.HEX) //HEX or DECIMAL; Note that (HSV || HSL || CMYK) && IndicatorMode.HEX is a bad idea
                .create()

        // This color picker does not play nice with kotlin. Maybe
        // TODO: Custom color picker impl
        colorPicker.onColorSelectedListener = object : OnColorSelectedListener {
            override fun onPositiveButtonClick(color: Int) {
                when(which) {
                    //Action bar
                    0 -> Aesthetic.get()
                                .colorPrimary(color)
                                .colorStatusBar(color - 987670)
                                .apply()
                    //Background
                    1 -> Aesthetic.get()
                            .colorWindowBackground(color)
                            .colorCardViewBackground(color + 987670)
                            .apply()
                    //Accent
                    2 -> Aesthetic.get()
                            .colorAccent(color)
                            .apply()
                }
                view.setBackgroundColor(color)
                colorPicker.dismiss()
            }

            override fun onNegativeButtonClick(color: Int) {
                // Good thing we had to override this method!!
                colorPicker.dismiss()
            }
        }
        colorPicker.show(fm, "Color Picker")
    }
}