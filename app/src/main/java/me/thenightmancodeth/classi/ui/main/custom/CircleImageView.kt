/**
 * This file is part of classi.

 * classi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * classi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with classi.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package me.thenightmancodeth.classi.ui.main.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.util.AttributeSet

import me.thenightmancodeth.classi.R


class CircleImageView(ctx: Context, attrs: AttributeSet) : android.support.v7.widget.AppCompatTextView(ctx, attrs) {
    private val backgroundColor = ContextCompat.getColor(context, R.color.colorAccent)
    private var circleTextColor: Int = 0
    private var strokeColor: Int = 0
    private var circlePaint: Paint? = null
    private var strokePaint: Paint? = null

    init {

        val a = ctx.theme.obtainStyledAttributes(
                attrs,
                R.styleable.CircleView,
                0, 0)

        try {
            circleTextColor = a.getColor(R.styleable.CircleView_text_color, ContextCompat.getColor(context, R.color.colorAccent))
            strokeColor = a.getColor(R.styleable.CircleView_border_color, Color.TRANSPARENT)
        } finally {
            a.recycle()
        }

        init()
    }

    private fun init() {
        val textPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        textPaint.color = circleTextColor

        circlePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        circlePaint!!.style = Paint.Style.FILL
        circlePaint!!.color = backgroundColor

        strokePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        strokePaint!!.style = Paint.Style.STROKE
        strokePaint!!.strokeWidth = 3.toFloat()
        strokePaint!!.color = strokeColor
    }

    override fun onDraw(canvas: Canvas) {
        val h = this.height
        val w = this.width

        val diameter = if (h > w) h else w
        val radius = diameter / 2

        this.height = diameter + 20
        this.width = diameter + 20

        canvas.drawCircle(radius.toFloat(), radius.toFloat(), radius.toFloat(), circlePaint!!)
        canvas.drawCircle((radius).toFloat(), (radius).toFloat(), (radius-2).toFloat(), strokePaint!!)

        super.onDraw(canvas)
    }

    override fun setBackgroundColor(bgColor: Int) {
        circlePaint!!.color = bgColor
        invalidate()
        requestLayout()
    }
}
