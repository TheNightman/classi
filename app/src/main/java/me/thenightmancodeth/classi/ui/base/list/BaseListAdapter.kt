/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.base.list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

abstract class BaseListAdapter<T> : RecyclerView.Adapter<BaseListAdapter<T>.ViewHolder>() {
    protected var items: MutableList<T> = ArrayList()

    abstract fun getListItemView(context: Context): BaseViewHolder<T>

    fun addItems(itemsToAdd: List<T>) {
        items.addAll(itemsToAdd)
        notifyItemRangeInserted(0, itemsToAdd.size)
    }

    fun clearItems() {
        val itemCount = items.size
        items.clear()
        notifyItemRangeRemoved(0, itemCount)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder = ViewHolder(getListItemView(viewGroup.context))
    //override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.view.bind(items[position])
    override fun getItemCount(): Int = items.size

    inner class ViewHolder(var view: BaseViewHolder<T>) : RecyclerView.ViewHolder(view)
}