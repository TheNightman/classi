/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.settings.theme.list

import android.content.Context
import android.view.View
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.ui.base.list.BaseViewHolder

class ThemeSettingsViewHolder(context: Context) : BaseViewHolder<Pair<Pair<String, String>, Pair<View.OnClickListener, Int>>>(context) {
    override fun layoutResId(): Int = R.layout.view_settings_theme

    override fun bind(item: Pair<Pair<String, String>, Pair<View.OnClickListener, Int>>) {

    }
}