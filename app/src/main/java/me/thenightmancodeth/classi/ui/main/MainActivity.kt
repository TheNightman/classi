/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.main

import android.app.TimePickerDialog
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.afollestad.aesthetic.Aesthetic
import io.realm.Realm
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.ui.base.BaseActivity
import me.thenightmancodeth.classi.ui.main.list.MainAdapter
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import me.thenightmancodeth.classi.data.model.Class
import me.thenightmancodeth.classi.ui.settings.main.SettingsActivity
import me.thenightmancodeth.classi.utilities.alarm.AlarmBootReceiver
import me.thenightmancodeth.classi.utilities.extensions.hide
import me.thenightmancodeth.classi.utilities.extensions.scheduleAlarm
import me.thenightmancodeth.classi.utilities.extensions.toDaysString

open class MainActivity : BaseActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var adapter: MainAdapter

    override fun getLayoutResId() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent?.inject(this)

        Realm.init(applicationContext)

        if(Aesthetic.isFirstTime()) {
            Aesthetic.get()
                    .colorPrimaryRes(R.color.colorPrimary)
                    .colorPrimaryRes(R.color.colorPrimaryDark)
                    .textColorSecondaryRes(R.color.textColorSecondary)
                    .textColorPrimaryRes(R.color.textColorPrimary)
                    .colorWindowBackgroundRes(R.color.white)
                    .colorAccentRes(R.color.colorAccent)
                    .isDark(false)
                    .apply()
        }

        setupToolbar()
        setupRecycler()
        setupSwipeToRefresh()
        setupFAB()

        presenter.bind(this)
    }

    private fun setupToolbar() {
        toolbarTitle.text = getString(R.string.classi)
        setSupportActionBar(toolbar as Toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.menu_main_setting -> {
                val settings = Intent(this@MainActivity, SettingsActivity::class.java)
                startActivity(settings)
            }
            R.id.menu_main_night -> {
                Aesthetic.get()
                        .textColorPrimaryRes(R.color.white)
                        .isDark(true)
                        .apply()
            }
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    private fun setupRecycler() {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val itemDec = DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
        itemDec.setDrawable(getDrawable(R.drawable.divider))
        recyclerView.addItemDecoration(itemDec)
        recyclerView.adapter = adapter
        presenter.fetchClassesFromRealm()
    }

    private fun setupSwipeToRefresh() {
        //TODO: Update from server
        swipeRefreshLayout.setOnRefreshListener { presenter.fetchClassesFromRealm() }
    }

    private fun setupFAB() {
        add_class_FAB.setOnClickListener {
            showClassDialog()
        }
    }

    private fun showClassDialog() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(MainActivity@this)
        val dialogView = View.inflate(applicationContext, R.layout.add_class_dialog, null)

        alertDialog.setView(dialogView)
        alertDialog.setTitle(getString(R.string.add_class_dialog_title))
        alertDialog.setCancelable(true)

        var hr: Int = 0
        var mn: Int = 0
        dialogView.findViewById<Button>(R.id.new_class_dialog_time_picker_button).setOnClickListener {
            TimePickerDialog(this@MainActivity, { view: TimePicker, hour: Int, min: Int ->
                hr = hour
                mn = min
            }, hr, mn, false).show()
        }

        alertDialog.setPositiveButton("Add", { dialog, _ ->
            //Enable the boot receiver
            val receiver: ComponentName = ComponentName(applicationContext,
                    AlarmBootReceiver::class.java)
            packageManager.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP)

            //Create RealmList from days
            val dayBoxes: List<CheckBox> = listOf(
                    dialogView.findViewById<CheckBox>(R.id.check_sun),
                    dialogView.findViewById<CheckBox>(R.id.check_mon),
                    dialogView.findViewById<CheckBox>(R.id.check_tue),
                    dialogView.findViewById<CheckBox>(R.id.check_wed),
                    dialogView.findViewById<CheckBox>(R.id.check_thu),
                    dialogView.findViewById<CheckBox>(R.id.check_fri),
                    dialogView.findViewById<CheckBox>(R.id.check_sat)
            )

            val clazz: Class = Class(
                    name = dialogView.findViewById<EditText>(R.id.new_class_dialog_name).text.toString(),
                    professor = dialogView.findViewById<EditText>(R.id.new_class_dialog_professor).text.toString(),
                    location = dialogView.findViewById<EditText>(R.id.new_class_dialog_location).text.toString(),
                    days = dayBoxes.toDaysString(),
                    time = "$hr:$mn")

            presenter.addClassToRealm(clazz)

            clazz.days.toCharArray().forEach {
                it.scheduleAlarm(applicationContext, clazz)
            }

            presenter.fetchClassesFromRealm()
            dialog.dismiss()
        })

        alertDialog.show()
    }

    override fun onFetchClassesSuccess(classes: List<Class>) {
        adapter.clearItems()
        adapter.addItems(classes)
        swipeRefreshLayout.isRefreshing = false
        statusText.hide()
    }

    override fun onFetchClassesError(error: Throwable) {
        Toast.makeText(this, "Error. ${error.message.toString()}", Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }
}
