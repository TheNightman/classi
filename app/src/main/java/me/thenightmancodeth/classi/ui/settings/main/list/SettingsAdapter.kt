/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.settings.main.list

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.injection.scope.PerActivity
import me.thenightmancodeth.classi.ui.base.list.BaseListAdapter
import me.thenightmancodeth.classi.ui.base.list.BaseViewHolder
import me.thenightmancodeth.classi.ui.settings.theme.ThemeSettingsActivity
import javax.inject.Inject

@PerActivity
class SettingsAdapter @Inject constructor() : BaseListAdapter<Pair<Pair<String, String>, Int>>() {
    override fun getListItemView(context: Context): BaseViewHolder<Pair<Pair<String, String>, Int>> {
        return SettingsViewHolder(context)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val setting = items[position]
        val bg: GradientDrawable? = holder?.view?.findViewById<ImageView>(R.id.settings_icon)
                ?.background?.mutate() as GradientDrawable?
        bg?.setColor(Color.RED)
        holder?.view?.findViewById<LinearLayout>(R.id.view_root)?.setOnClickListener {
            when(setting.first.first) {
                "Look and Feel" ->  {
                    holder.view.context.startActivity(Intent(holder.view.context,
                            ThemeSettingsActivity::class.java))
                }
                else -> {
                    Toast.makeText(holder.view.context, "Coming soon!", Toast.LENGTH_LONG).show()
                }
            }

        }
        holder?.view?.findViewById<ImageView>(R.id.settings_icon)
                ?.setImageResource(setting.second)
        holder?.view?.findViewById<TextView>(R.id.list_item_title)
                ?.text = setting.first.first
        holder?.view?.findViewById<TextView>(R.id.list_item_details_lv1)
                ?.text = setting.first.second
    }
}