/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.settings.theme

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Toast
import com.afollestad.aesthetic.Aesthetic
import kotlinx.android.synthetic.main.activity_theme.*
import kotlinx.android.synthetic.main.toolbar.*
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.ui.base.BaseActivity
import me.thenightmancodeth.classi.ui.settings.theme.list.ThemeSettingsAdapter
import javax.inject.Inject
import com.kunzisoft.androidclearchroma.IndicatorMode
import com.kunzisoft.androidclearchroma.colormode.ColorMode
import com.kunzisoft.androidclearchroma.ChromaDialog
import com.kunzisoft.androidclearchroma.listener.OnColorSelectedListener
import me.thenightmancodeth.classi.ui.main.custom.CircleImageView


class ThemeSettingsActivity: BaseActivity(), ThemeSettingsView {
    @Inject
    lateinit var presenter: ThemeSettingsPresenter

    @Inject
    lateinit var adapter: ThemeSettingsAdapter

    override fun getLayoutResId() = R.layout.activity_theme

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent?.inject(this)

        setupToolbar()
        setupRecyclerView()

        presenter.bind(this)
    }

    private fun setupToolbar() {
        toolbarTitle.text = getString(R.string.settings_theme_title)
        setSupportActionBar(toolbar as Toolbar)
    }

    private fun setupRecyclerView() {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = adapter

        val colorPicker = ChromaDialog.Builder()
                    .initialColor(Aesthetic.get().colorPrimary().take(1).blockingFirst())
                    .colorMode(ColorMode.RGB) // RGB, ARGB, HVS, CMYK, CMYK255, HSL
                    .indicatorMode(IndicatorMode.HEX) //HEX or DECIMAL; Note that (HSV || HSL || CMYK) && IndicatorMode.HEX is a bad idea
                    .create()

        val settings = listOf(
                Pair(Pair("Action Bar", "Change the color of the title bar"),
                        Pair(View.OnClickListener { view ->
                            presenter.showColorPicker(supportFragmentManager,
                                    view.findViewById(R.id.list_item_circle), 0)
                        }, Aesthetic.get().colorPrimary().take(1).blockingFirst())),
                Pair(Pair("Background", "Change the color of the background"),
                        Pair(View.OnClickListener { view ->
                            presenter.showColorPicker(supportFragmentManager,
                                    view.findViewById(R.id.list_item_circle), 1)
                        }, Aesthetic.get().colorWindowBackground().take(1).blockingFirst())),
                Pair(Pair("Accent Color", "Change the accent color"),
                        Pair(View.OnClickListener { view ->
                            presenter.showColorPicker(supportFragmentManager,
                                    view.findViewById(R.id.list_item_circle), 2)
                        }, Aesthetic.get().colorAccent().take(1).blockingFirst()))
        )
        presenter.getThemeSettings(settings)
    }

    override fun onFetchThemeSettingsSuccess(settings: List<Pair<Pair<String, String>,
            Pair<View.OnClickListener, Int>>>) {
        adapter.clearItems()
        adapter.addItems(settings)
    }

    override fun onFetchThemeSettingsError(error: Throwable) {
        Toast.makeText(this, "Error. ${error.message.toString()}", Toast.LENGTH_LONG).show()
    }
}