/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.settings.theme.list

import android.content.Context
import android.view.View
import android.widget.TextView
import com.afollestad.aesthetic.Aesthetic
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.injection.scope.PerActivity
import me.thenightmancodeth.classi.ui.base.list.BaseListAdapter
import me.thenightmancodeth.classi.ui.base.list.BaseViewHolder
import me.thenightmancodeth.classi.ui.main.custom.CircleImageView
import javax.inject.Inject

@PerActivity
class ThemeSettingsAdapter @Inject constructor() : BaseListAdapter<Pair<Pair<String, String>, Pair<View.OnClickListener, Int>>>() {
    override fun getListItemView(context: Context): BaseViewHolder<Pair<Pair<String, String>, Pair<View.OnClickListener, Int>>> {
        return ThemeSettingsViewHolder(context)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val setting = items[position]
        holder?.view?.findViewById<CircleImageView>(R.id.list_item_circle)
                ?.setBackgroundColor(setting.second.second)
        holder?.view?.findViewById<CircleImageView>(R.id.list_item_circle)
                ?.setOnClickListener(setting.second.first)
        holder?.view?.findViewById<TextView>(R.id.list_item_title)?.text = setting.first.first
        holder?.view?.findViewById<TextView>(R.id.list_item_details_lv1)?.text = setting.first.second
    }
}