/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.main.list

import android.app.AlertDialog
import android.content.Context
import android.widget.LinearLayout
import android.widget.TextView
import io.realm.Realm
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.data.model.Class
import me.thenightmancodeth.classi.injection.scope.PerActivity
import me.thenightmancodeth.classi.ui.base.list.BaseListAdapter
import me.thenightmancodeth.classi.ui.base.list.BaseViewHolder
import me.thenightmancodeth.classi.ui.main.custom.CircleImageView
import me.thenightmancodeth.classi.utilities.extensions.to12Hr
import javax.inject.Inject

@PerActivity
class MainAdapter @Inject constructor() : BaseListAdapter<Class>() {
    override fun getListItemView(context: Context): BaseViewHolder<Class> {
        return ClassViewHolder(context)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val clazz: Class = items[position]
        holder?.view?.findViewById<CircleImageView>(R.id.list_item_circle)?.text =
                clazz.grade.toString()
        holder?.view?.findViewById<TextView>(R.id.list_item_title)?.text = clazz.name
        val details = "${clazz.professor} | ${clazz.location} | ${clazz.days} - ${clazz.time.to12Hr()}"
        holder?.view?.findViewById<TextView>(R.id.list_item_details_lv1)?.text = details

        holder?.view?.findViewById<LinearLayout>(R.id.list_item_root)?.setOnLongClickListener {
            val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(holder.view.context)
            dialogBuilder.setTitle(R.string.delete_confirm_title)
            dialogBuilder.setMessage(R.string.delete_confirm_body)
            dialogBuilder.setPositiveButton("YES", { dialog, _ ->
                val realm: Realm = Realm.getDefaultInstance()
                realm.use {
                    it.executeTransaction {
                        clazz.deleteFromRealm()
                    }
                }
                items.removeAt(position)
                this.notifyItemRemoved(position)
                dialog.dismiss()
            })
            dialogBuilder.setNegativeButton("NO", { dialog, _ ->
                dialog.dismiss()
            })
            dialogBuilder.show()

            true
        }
    }
}