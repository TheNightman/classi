/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.main

import me.thenightmancodeth.classi.data.DataManager
import me.thenightmancodeth.classi.injection.scope.PerActivity
import me.thenightmancodeth.classi.ui.base.BasePresenter
import javax.inject.Inject
import io.reactivex.schedulers.Schedulers.io
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import me.thenightmancodeth.classi.data.model.Class

@PerActivity
class MainPresenter @Inject constructor(val dataManager: DataManager) : BasePresenter<MainView>() {
    /**
     * Adds Classes from API to disposables
     * TODO: Add the classes to realm
     */
    fun fetchClassesFromServer() {
        disposables.add(
                dataManager.getUserClasses("thenightman:159753")
                        .subscribeOn(io())
                        .observeOn(mainThread())
                        .subscribe(
                                { onFetchClassesSuccess(it) },
                                { onFetchClassesError(it) }
                        )
        )
    }

    /**
     * Adds Classes from realm to disposables
     */
    fun fetchClassesFromRealm() {
        disposables.add(
                dataManager.getClassesFromRealm()
                        .subscribeOn(io())
                        .observeOn(mainThread())
                        .subscribe(
                                { onFetchClassesSuccess(it) },
                                { onFetchClassesError(it) }
                        )
        )
    }

    fun addClassToRealm(clazz: Class) {
        dataManager.addClassToRealm(clazz)
    }

    fun onFetchClassesSuccess(classes: List<Class>) {
        view?.onFetchClassesSuccess(classes)
    }

    fun onFetchClassesError(error: Throwable) {
        view?.onFetchClassesError(error)
    }
}