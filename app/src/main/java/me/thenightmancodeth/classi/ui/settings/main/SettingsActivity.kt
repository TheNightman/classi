/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.ui.settings.main

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.toolbar.*
import me.thenightmancodeth.classi.R
import me.thenightmancodeth.classi.ui.base.BaseActivity
import me.thenightmancodeth.classi.ui.settings.main.list.SettingsAdapter
import javax.inject.Inject

open class SettingsActivity : BaseActivity(), SettingsView {
    @Inject
    lateinit var presenter: SettingsPresenter

    @Inject
    lateinit var adapter: SettingsAdapter

    override fun getLayoutResId(): Int = R.layout.activity_settings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent?.inject(this)
        setupToolbar()
        setupRecycler()
        presenter.bind(this)
    }

    private fun setupToolbar() {
        toolbarTitle.text = getString(R.string.settings)
        setSupportActionBar(toolbar as Toolbar)
    }

    private fun setupRecycler() {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = adapter
        presenter.getSettings()
    }

    override fun onFetchSettingsSuccess(settings: List<Pair<Pair<String, String>, Int>>) {
        adapter.clearItems()
        adapter.addItems(settings)
    }

    override fun getContext(): Context {
        return this@SettingsActivity
    }

    override fun onFetchSettingsError(error: Throwable) {
        Toast.makeText(this, "Error. ${error.message.toString()}", Toast.LENGTH_LONG).show()
    }
}