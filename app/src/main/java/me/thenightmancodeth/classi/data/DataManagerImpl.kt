/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.data

import io.reactivex.Observable
import io.realm.Realm
import io.realm.RealmResults
import me.thenightmancodeth.classi.data.model.Class
import me.thenightmancodeth.classi.data.remote.ClassService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class DataManagerImpl @Inject constructor(val classService: ClassService) : DataManager {
    override fun getUserClasses(auth: String): Observable<List<Class>> {
        return classService
                .fetchUserClasses(0) //TODO: Get user ID
                .flatMap { Observable.just(it.value) }
    }

    override fun getClassesFromRealm(): Observable<RealmResults<Class>> {
        return Observable.just(Realm.getDefaultInstance().where(Class::class.java).findAll())
    }

    override fun deleteClassFromRealm(clazz: Class) {
        val realmInstance: Realm = Realm.getDefaultInstance()
        realmInstance.use {
            it.executeTransaction {
                clazz.deleteFromRealm()
            }
        }
    }

    override fun addClassToRealm(clazz: Class) {
        val realmInstance: Realm = Realm.getDefaultInstance()
        realmInstance.use {
            it.executeTransaction { realm ->
                realm.insertOrUpdate(clazz)
            }
        }
    }
}