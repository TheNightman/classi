/**
 * This file is part of classi.
 *
 *  classi is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  classi is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with classi.  If not, see <http://www.gnu.org/licenses/>.
 **/
package me.thenightmancodeth.classi.data.remote

import io.reactivex.Observable
import me.thenightmancodeth.classi.data.model.Class
import me.thenightmancodeth.classi.data.model.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ClassService {
    @GET("/api/{user}/classes")
    fun fetchUserClasses(@Path("user") user: Int): Observable<Response<Class>>
}