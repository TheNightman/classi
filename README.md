# Classi - Android college assistant
This repo contains the Kotlin/MVP re-write of Classi for Android

## Building
Open it up in Studio and run a build

## Features
* Short-term
  * Grade tracking
  * Scheduling
  * Alerts

* Long-term
  * Social - Friends/Classes/Groups
  * 

## Contributing
We're accepting pull requests! Checkout the backend/server code
[here](https://gitlab.com/joyod3/classi-API)

